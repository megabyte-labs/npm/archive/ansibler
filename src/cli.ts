/* eslint-disable no-console */
import _optionator from 'optionator'

import { App } from './app'
import { CLI_OPTIONS } from './constants/cli-options.constant'
import { Logger } from './lib/log'

const optionator = _optionator(CLI_OPTIONS)

const EXIT_0 = 0
const EXIT_1 = 1

type ILooseObject = {
  readonly [key: string]: readonly string[]
}

/**
 * Verify enough command line options have provided
 *
 * @param {ILooseObject} options command line arguments
 * @returns {boolean} Returns true if enough arguments provided or false otherwise
 */
function noOptionsSet(options: ILooseObject): boolean {
  const optionsLength = Object.keys(options).length
  const minOptions = 3
  if (optionsLength < minOptions) {
    return true
  }
  if (optionsLength === minOptions && options.output && options._) {
    return true
  }

  return false
}

/**
 * This is where the logic that handles the CLI should reside
 *
 * @param {any} argz The command line arguments passed to the program
 * @returns {number} Exit status
 */
export async function cli(argz: unknown): Promise<number> {
  let options
  try {
    options = optionator.parseArgv(argz)
  } catch (error) {
    // pretty print errors
    console.log(error.message)
    console.log('')

    return EXIT_1
  }

  // asked for help or no options set?
  if (options.help || noOptionsSet(options)) {
    console.log(optionator.generateHelp())

    return EXIT_0
  }

  try {
    if (options.populateDescriptions) {
      const { roles, output, username, baseUrl, moleculeFilename } = options
      await App.populateDescriptions(roles, output, baseUrl, username, moleculeFilename)
    }
    if (options.populatePlatforms) {
      await App.populatePlatforms(options.roles, options.moleculeFilename)
    }
    if (options.populateRoleDependencies) {
      await App.populateRoleDependencies(options.roles, options.username)
    }
  } catch (error) {
    Logger.error(error)
  }

  return EXIT_0
}
