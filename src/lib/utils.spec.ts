import test from 'ava'

import { getPlatformVersionName } from './utils'

test('getPlatformVersionName with no platform name', async (t) => {
  const code = await getPlatformVersionName('platform', '10.10001')
  t.is(code, undefined)
})

test('getPlatformVersionName with platform name', async (t) => {
  const code = await getPlatformVersionName('Ubuntu', '14.04')
  t.is(code, 'trusty')
  const code2 = await getPlatformVersionName('MacOS', '11')
  t.is(code2, 'Big Sur')
})
