import fs from 'fs'
import path from 'path'
import { promisify } from 'util'

import { validate } from 'class-validator'
import _glob from 'glob'
import { readFile, writeFile } from 'jsonfile'
import yaml from 'yaml'

import { extractData } from './lib/helper'
import { Logger } from './lib/log'
import {
  buildRoleDependencyMap,
  findDependencyRoleLocations,
  findRequirementsYAMLs,
  getDependencyCart,
  getMissingDependencies,
  getUniqueDependencies,
  injectRoleDependencies
} from './lib/role-dependency-helper'
import { formatValidationErrors, generateRoles, getPlatformAlias, getPlatformVersionName } from './lib/utils'
import { PopulateDescriptionsOptions } from './models/populate-descriptions.model'
import { PopulatePlatformsOptions } from './models/populate-platforms-options.model'
import { PopulateRoleDependencyOptions } from './models/populate-role-dependency-options.model'

const glob = promisify(_glob)

/**
 * Ansibler main module
 */
export class App {
  private static async findMainYAMLs(roles: string): Promise<readonly string[]> {
    // look for meta/main.yml from the given role path
    const files = await glob(`${roles}/*/*/meta/main.yml`)

    return files
  }

  /**
   * Format the data to write into file
   *
   * @param {string[][]} data Data extracted from the roles
   * @param {string} baseUrl GitLab base URL
   * @param {string} username GitLab username
   * @returns {string[][]} Formatted data
   */
  private static format(
    data: readonly (readonly (string | readonly string[])[])[],
    baseUrl: string,
    username: string
  ): readonly (readonly (string | readonly string[])[])[] {
    const url = baseUrl[baseUrl.length - 1] === '/' ? baseUrl : baseUrl + '/'

    return data.map((row) => [
      `<a href='${url}${row[0]}'>${username}.${row[0]}</a>`,
      row[1],
      row[2],
      `<a href="${url}${row[0]}"><img src="${url}${row[0]}/badges/master/pipeline.svg" /></a>`
    ])
  }

  /**
   * Extract supported OSes from the given data (compatibility matrix)
   *
   * It will return the OS image urls as array that have passed in the compatibility matrix.
   * Image urls point to the GitLab assets project eg. https://gitlab.com/megabyte-labs/assets/-/raw/master/icon/ubuntu.png
   *
   * @param {string[][]} data Data from the Ansible molecule json compatibility matrix
   * @returns {string[]} Returns supported OS names as array
   */
  private static getSupportedOSes(data: readonly (readonly string[])[]): readonly string[] {
    return [
      ...new Set( // remove duplicates
        data
          .filter((row) => row[2] === '✅') // only include status ok OSes
          .map(
            (row) =>
              `https://gitlab.com/megabyte-labs/assets/-/raw/master/icon/${row[0]
                .replaceAll(' ', '') // remove spaces from name
                .toLowerCase()}.png`
          )
      )
    ]
  }

  /**
   * Write data to the given file
   *
   * @param {string} filePath Write given data to this file
   * @param {string[][]} data Data to write to the file
   * @returns {void}
   */
  private static async writeToFile(
    filePath: string,
    data: readonly (readonly (string | readonly string[])[])[]
  ): Promise<void> {
    // write to file
    Logger.info(`Writing to file ${filePath}`)

    const result = await writeFile(
      filePath,
      {
        role_dependencies: [['Role Dependency', 'Description', 'Supported OSes', 'Status'], ...data]
      },
      { spaces: 2 }
    )

    return result
  }

  /**
   * Extract compatibility matrix from the given moleculeFilename file
   *
   * moleculeFilename should exist at the root of the particular ansible role directory.
   * This function returns only the data without header row from the matrix
   *
   * @param {string} yamlFilePath File path to main.yaml
   * @param {string} moleculeFilename JSON file name generated by the ansible-molecule-json tool
   * @returns {string[][]} Compatibility matrix
   */
  private static async getCompatibilityMatrix(
    yamlFilePath: string,
    moleculeFilename: string
  ): Promise<readonly (readonly string[])[]> {
    const baseDirectory = path.dirname(yamlFilePath)
    const moleculeFilePath = path.join(baseDirectory, '/../', moleculeFilename)

    // is molecule json file does not exist?
    if (!fs.existsSync(moleculeFilePath)) {
      Logger.warn(`Compatibility matrix cannot be found at ${moleculeFilePath}`)

      return []
    }

    const moleculeJSON = await readFile(moleculeFilePath)
    // remove header array
    moleculeJSON.compatibility.shift()

    return moleculeJSON.compatibility
  }

  /**
   * Inject compatibility to main.yaml file
   *
   * @param {string} yamlFilePath main.yaml file path
   * @param {string} moleculeFilename molecule filename
   * @returns {Promise<void>}
   */
  private static async injectCompatibility(yamlFilePath: string, moleculeFilename: string): Promise<void> {
    const baseDirectory = path.dirname(yamlFilePath)
    const moleculeFilePath = path.join(baseDirectory, '/../', moleculeFilename)

    // is molecule json file exists?
    let compatibilities
    try {
      compatibilities = await this.getCompatibilityMatrix(yamlFilePath, moleculeFilename)
    } catch (error) {
      Logger.error(error)

      return
    }

    const MIN_LENGTH = 1
    if (compatibilities.length < MIN_LENGTH) {
      Logger.warn(`There is no enough compatibility data on ${moleculeFilePath}`)

      return
    }

    const yml = yaml.parse(fs.readFileSync(yamlFilePath, { encoding: 'utf8' }))
    if (!yml.galaxy_info.platforms) {
      // eslint-disable-next-line functional/immutable-data
      yml.galaxy_info.platforms = []
    }

    // group compatibilities
    const groups = await compatibilities.reduce(async (previous, row) => {
      const previous_ = await previous

      // is status not ok?
      if (row[2] !== '✅') {
        // then don't include this version
        return previous_
      }

      const name = await getPlatformVersionName(row[0], row[1])
      const osName = await getPlatformAlias(row[0])
      const codeName = name ? name : row[1]
      if (previous_[osName]) {
        previous_[osName].push(codeName)
      } else {
        // eslint-disable-next-line functional/immutable-data
        previous_[osName] = [codeName]
      }

      return previous_
    }, {})

    // eslint-disable-next-line functional/immutable-data
    yml.galaxy_info.platforms = Object.keys(groups).map((key) => {
      return {
        name: key,
        versions: groups[key] ?? 'Latest'
      }
    })

    // write the yaml file
    fs.writeFileSync(yamlFilePath, yaml.stringify(yml), { encoding: 'utf8' })
    Logger.info(`Updated ${yamlFilePath}`)
  }

  /**
   * Extract data from the given Ansible roles and output the data in to a given file
   *
   * @param {string} roles Directory where Ansible roles are located, relative to the current working directory
   * @param {string} output Relative file path of the data should write to
   * @param {string} baseUrl GitLab base url
   * @param {string} username GitLab username
   * @param {string} moleculeFilename Filename of the output of ansible-molecule-json tool
   */
  public static async populateDescriptions(
    roles: string,
    output: string,
    baseUrl: string,
    username: string,
    moleculeFilename: string
  ): Promise<void> {
    // validate parameters
    const config = new PopulateDescriptionsOptions({ baseUrl, moleculeFilename, output, roles, username })
    const errors = await validate(config)
    if (errors.length > 0) {
      throw new Error(formatValidationErrors(errors))
    }

    const absRolesPath = path.isAbsolute(roles) ? roles : path.resolve(process.cwd(), roles)
    const absOutput = path.isAbsolute(output) ? output : path.resolve(process.cwd(), output)

    // find
    const files = await this.findMainYAMLs(absRolesPath)
    Logger.info(`Found ${files.length} roles`)

    // read meta/main.yml file and extract descriptions
    const data = await Promise.all(
      files.map(async (file) => {
        const extracted = await extractData(file)

        // add supported OSes for this ansible role
        const compatibilities = await this.getCompatibilityMatrix(file, moleculeFilename)
        const supported = this.getSupportedOSes(compatibilities)

        return [...extracted, supported]
      })
    )
    // build data
    const formattedData = this.format(data, baseUrl, username)

    // output
    await this.writeToFile(absOutput, formattedData)
  }

  /**
   * Populate platform data to main.yaml file of each Ansible roles
   *
   * @param {string} roles Directory where Ansible roles are located.
   * @param {string} moleculeFilename Ansible molecule JSON file name
   */
  public static async populatePlatforms(roles: string, moleculeFilename: string): Promise<void> {
    const config = new PopulatePlatformsOptions({ moleculeFilename, roles })
    const errors = await validate(config)
    if (errors.length > 0) {
      throw new Error(formatValidationErrors(errors))
    }

    const absRoles = path.isAbsolute(roles) ? roles : path.resolve(process.cwd(), roles)
    const files = await this.findMainYAMLs(absRoles)
    Logger.info(`Found ${files.length} roles`)

    await Promise.all(
      files.map(async (file) => {
        await this.injectCompatibility(file, moleculeFilename)
      })
    )
  }

  /**
   * Populate dependencies for individual role inside the roles directory
   *
   * It will inject dependency chart to the package.json found on each role as "requirements"
   *
   *
   * @param {string} roles Roles directory path
   * @param {string} username Username to be use as the namespace of ansible roles
   */
  public static async populateRoleDependencies(roles: string, username: string): Promise<void> {
    // validate arguments
    const config = new PopulateRoleDependencyOptions({ roles, username })
    const errors = await validate(config)
    if (errors.length > 0) {
      throw new Error(formatValidationErrors(errors))
    }

    // find requirements.yml files
    const requirementYAMLs = await findRequirementsYAMLs(roles)
    Logger.info(`Found ${requirementYAMLs.length} roles`)

    // read every requirements.yml file and build dependency map
    const roleDependencyMap = buildRoleDependencyMap(requirementYAMLs)
    const dependenciesSet = getUniqueDependencies(roleDependencyMap)

    Logger.info(`Building map of role directories`)

    const roleLocationsMap = await findDependencyRoleLocations(username, dependenciesSet)

    const missingDeps = getMissingDependencies(dependenciesSet, roleLocationsMap)

    if (missingDeps.length > 0) {
      Logger.log(`There are ${missingDeps.length} dependencies to generate`)

      await generateRoles(username, missingDeps)

      Logger.log('Generated missing dependencies')
    } else {
      Logger.log('All dependencies found')
    }

    // build the depsChartMap
    Logger.log('Building dependency charts')

    const dependencyChartMap = await getDependencyCart(roleLocationsMap)

    Logger.log('Injecting dependencies chart')

    // write to file
    injectRoleDependencies(roleDependencyMap, dependencyChartMap)

    Logger.info('Done')
  }
}
