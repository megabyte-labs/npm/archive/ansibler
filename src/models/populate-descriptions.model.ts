import { IsNotEmpty, IsString, IsUrl } from 'class-validator'

import { IsDirectory, IsWritable } from '../lib/validators/fs-validators'

/**
 * The master data model that includes
 */
export class PopulateDescriptionsOptions {
  @IsDirectory('roles', {
    message: 'roles must be a valid directory'
  })
  public readonly roles: string

  @IsWritable('output', {
    message: 'output must be writable'
  })
  public readonly output: string

  @IsUrl()
  public readonly baseUrl: string

  @IsString()
  @IsNotEmpty()
  public readonly username: string

  @IsString()
  @IsNotEmpty()
  public readonly moleculeFilename: string

  constructor(options: PopulateDescriptionsOptions) {
    this.roles = options.roles
    this.output = options.output
    this.baseUrl = options.baseUrl
    this.username = options.username
    this.moleculeFilename = options.moleculeFilename
  }
}
