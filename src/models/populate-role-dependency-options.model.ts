import { IsNotEmpty, IsString } from 'class-validator'

import { IsDirectory } from '../lib/validators/fs-validators'

export class PopulateRoleDependencyOptions {
  @IsDirectory('roles', {
    message: 'roles must be a valid directory'
  })
  public readonly roles: string

  @IsNotEmpty()
  @IsString()
  public readonly username: string

  constructor(options: PopulateRoleDependencyOptions) {
    this.roles = options.roles
    this.username = options.username
  }
}
