/* eslint-disable no-magic-numbers */
import fs from 'fs'
import os from 'os'
import path from 'path'
import { promisify } from 'util'

import test from 'ava'
import json from 'jsonfile'
import yaml from 'yaml'

const readFile = promisify(fs.readFile)

import { App } from './app'

// eslint-disable-next-line functional/prefer-readonly-type
const temporaryDirectories: string[] = []

/**
 * Get random file name
 *
 * @returns {string} random json file name
 */
const getRandomName = (): string => `${(Math.random().toString(36) + Math.random().toString(36)).slice(2, 7)}`

/**
 * Create temporary directory inside the OS temporary directory
 *
 * @returns {string} Returns temporary directory absolute path
 */
const createTemporaryDirectory = (): string => {
  const directory = path.join(os.tmpdir(), getRandomName())
  fs.mkdirSync(directory, { recursive: true })

  return directory
}

/**
 * Create random file
 *
 * @param {any} role Role name
 * @param {any} data Data to include in a file
 * @param {any} ansible Ansible data to include in a file
 * @returns {string} File path of the created file
 */
function createTemporaryFile(role: string, data: unknown, ansible: unknown): string {
  const temporaryDirectory = createTemporaryDirectory()
  const rolePath = path.join(temporaryDirectory, '/roles/application', role)
  const mainYaml = path.join(rolePath, '/meta/main.yml')
  fs.mkdirSync(path.dirname(mainYaml), { recursive: true })
  // eslint-disable-next-line functional/immutable-data
  temporaryDirectories.push(temporaryDirectory)
  fs.writeFileSync(mainYaml, yaml.stringify(data), { encoding: 'utf8' })

  if (ansible !== undefined) {
    json.writeFileSync(path.join(rolePath, 'ansible-molecule.json'), ansible)
  }

  return temporaryDirectory
}

const seedRoles = (role: string, { username, compatibilityMatrix = true }): readonly (string | unknown)[] => {
  const data = {
    dependencies: [],
    galaxy_info: {
      author: 'ProfessorManhattan',
      company: 'Megabyte Labs',
      description: `Installs ${role} on nearly any platform`,
      galaxy_tags: ['filter', 'grep', 'cli'],
      license: 'license (MIT)',
      min_ansible_version: 2.1,
      platforms: [
        {
          name: 'EL',
          versions: [6, 7, 8]
        },
        {
          name: 'Fedora',
          versions: ['all']
        },
        {
          name: 'Ubuntu',
          versions: ['all']
        },
        {
          name: 'Debian',
          versions: ['all']
        },
        {
          name: 'MacOS',
          versions: ['all']
        },
        {
          name: 'Windows',
          versions: [10]
        }
      ]
    }
  }

  const ansible = {
    compatibility: [
      ['OS Family', 'OS Version', 'Status', 'Idempotent'],
      ['Archlinux', 'latest', '❌', '❌'],
      ['CentOS', 7, '✅', '❌'],
      ['CentOS', 8, '✅', '❌'],
      ['Debian', 10, '✅', '❌'],
      ['Debian', 9, '❌', '❌'],
      ['Ubuntu', 18.04, '✅', '❌'],
      ['Ubuntu', 20.04, '✅', '❌']
    ]
  }

  const compatibility = {
    role_dependencies: [
      ['Role Dependency', 'Description', 'Supported OSes', 'Status'],
      [
        `<a href='https://gitlab.com/megabyte-space/ansible-roles/${role}'>${username}.${role}</a>`,
        `Installs ${role} on nearly any platform`,
        compatibilityMatrix
          ? [
              'https://gitlab.com/megabyte-labs/assets/-/raw/master/icon/centos.png',
              'https://gitlab.com/megabyte-labs/assets/-/raw/master/icon/debian.png',
              'https://gitlab.com/megabyte-labs/assets/-/raw/master/icon/ubuntu.png'
            ]
          : [],
        `<a href="https://gitlab.com/megabyte-space/ansible-roles/${role}"><img src="https://gitlab.com/megabyte-space/ansible-roles/${role}/badges/master/pipeline.svg" /></a>`
      ]
    ]
  }

  const finalData = {
    dependencies: [],
    galaxy_info: {
      author: 'ProfessorManhattan',
      company: 'Megabyte Labs',
      description: `Installs ${role} on nearly any platform`,
      galaxy_tags: ['filter', 'grep', 'cli'],
      license: 'license (MIT)',
      min_ansible_version: 2.1,
      platforms: [
        {
          name: 'EL',
          versions: [7, 8]
        },
        {
          name: 'Debian',
          versions: ['buster']
        },
        {
          name: 'Ubuntu',
          versions: ['bionic', 'focal']
        }
      ]
    }
  }

  return [createTemporaryFile(role, data, compatibilityMatrix ? ansible : undefined), finalData, compatibility]
}

test('Test populateDescriptions arguments - roles', async (t) => {
  const roles = 'this-path-does-not-exist'
  const output = '/tmp/out.json'
  const baseUrl = 'https://gitlab.com/megabyte-space/ansible-roles'
  const username = 'professormanhattan'
  const moleculeFilename = 'molecule.json'

  await t.throwsAsync(
    async () => {
      await App.populateDescriptions(roles, output, baseUrl, username, moleculeFilename)
    },
    {
      message: 'roles must be a valid directory'
    }
  )

  const rolesPath2 = __filename // eslint-disable-line unicorn/prefer-module
  await t.throwsAsync(
    async () => {
      await App.populateDescriptions(rolesPath2, output, baseUrl, username, moleculeFilename)
    },
    {
      message: 'roles must be a valid directory'
    }
  )
})

test('Test populateDescriptions arguments - output', async (t) => {
  const roles = '/tmp'
  const output = '/tmp/path/does/not/exist'
  const baseUrl = 'https://gitlab.com/megabyte-space/ansible-roles'
  const username = 'professormanhattan'
  const moleculeFilename = 'molecule.json'

  await t.throwsAsync(
    async () => {
      await App.populateDescriptions(roles, output, baseUrl, username, moleculeFilename)
    },
    {
      message: 'output must be writable'
    }
  )
})

test('Test populateDescriptions arguments - baseUrl', async (t) => {
  const roles = '/tmp'
  const output = '/tmp/path/does/not/exist'
  const baseUrl = 'ansible-roles'
  const username = 'professormanhattan'
  const moleculeFilename = 'molecule.json'

  await t.throwsAsync(
    async () => {
      await App.populateDescriptions(roles, output, baseUrl, username, moleculeFilename)
    },
    {
      message: 'output must be writable\nbaseUrl must be an URL address'
    }
  )
})

test('Test populateDescriptions arguments - username', async (t) => {
  const roles = '/tmp'
  const output = '/tmp/path/does/not/exist'
  const baseUrl = 'https://gitlab.com/megabyte-space/ansible-roles'
  const username = ''
  const moleculeFilename = 'molecule.json'

  await t.throwsAsync(
    async () => {
      await App.populateDescriptions(roles, output, baseUrl, username, moleculeFilename)
    },
    {
      message: 'output must be writable\nusername should not be empty'
    }
  )
})

test('Test populateDescriptions arguments - moleculeFilename', async (t) => {
  const roles = createTemporaryDirectory()
  const output = '/tmp/path/does/not/exist'
  const baseUrl = 'https://gitlab.com/megabyte-space/ansible-roles'
  const username = 'user'
  const moleculeFilename = ''

  await t.throwsAsync(
    async () => {
      await App.populateDescriptions(roles, output, baseUrl, username, moleculeFilename)
    },
    {
      message: 'output must be writable\nmoleculeFilename should not be empty'
    }
  )
})

test('Test no main.yml files in the roles path', async (t) => {
  const roles = createTemporaryDirectory()
  const output = '/tmp/output.json'
  const baseUrl = 'https://gitlab.com/megabyte-space/ansible-roles'
  const username = 'professormanhattan'
  const moleculeFilename = 'molecule.json'

  await t.is(await App.populateDescriptions(roles, output, baseUrl, username, moleculeFilename), undefined)

  // verify data
  const expected = {
    role_dependencies: [['Role Dependency', 'Description', 'Supported OSes', 'Status']]
  }

  const data = await readFile(output, { encoding: 'utf8' })
  t.like(JSON.parse(data), expected)
})

test('Test populate description output', async (t) => {
  const roleName = 'peco1'
  const [roles, , expected] = await seedRoles(roleName, { username: 'professormanhattan' })
  const moleculeFilename = 'ansible-molecule.json'
  const output = `/tmp/${getRandomName()}.json`
  const baseUrl = 'https://gitlab.com/megabyte-space/ansible-roles'
  const username = 'professormanhattan'

  await App.populateDescriptions(`${roles}/roles`, output, baseUrl, username, moleculeFilename)

  const data = await readFile(output, { encoding: 'utf8' })
  t.like(JSON.parse(data), expected as never)
})

test('Test populate description output without compatibility matrix', async (t) => {
  const roleName = 'peco2'
  const [roles, , expected] = await seedRoles(roleName, { compatibilityMatrix: false, username: 'professormanhattan' })
  const moleculeFilename = 'ansible-molecule.json'
  const output = `/tmp/${getRandomName()}.json`
  const baseUrl = 'https://gitlab.com/megabyte-space/ansible-roles'
  const username = 'professormanhattan'

  await App.populateDescriptions(`${roles}/roles`, output, baseUrl, username, moleculeFilename)

  const data = await readFile(output, { encoding: 'utf8' })
  t.like(JSON.parse(data), expected as never)
})

test('Test populate platforms return type', async (t) => {
  const roles = '/tmp'
  const moleculeFilename = 'molecule.json'
  await t.is(await App.populatePlatforms(roles, moleculeFilename), undefined)
})

test('Test populatePlatforms arguments', async (t) => {
  const roles = '/tmp/foo/bar'
  const moleculeFilename = 'molecule.json'
  await t.throwsAsync(
    async () => {
      await App.populatePlatforms(roles, moleculeFilename)
    },
    {
      message: 'roles must be a valid directory'
    }
  )
})

test('Test populatePlatforms', async (t) => {
  const roleName = 'peco3'
  const [roles, expected] = await seedRoles(roleName, { username: 'professormanhattan' })
  const moleculeFilename = 'ansible-molecule.json'
  await t.is(await App.populatePlatforms(path.join(roles as string, 'roles'), moleculeFilename), undefined)

  // verify update
  const mainYamlPath = path.join(roles as string, 'roles/application', roleName, 'meta/main.yml')
  const stringData = fs.readFileSync(mainYamlPath, {
    encoding: 'utf8'
  })
  const yamlData = yaml.parse(stringData)
  t.deepEqual(yamlData, expected)
})

test.after.always('cleaning up', (): void => {
  // delete tempFiles
  if (temporaryDirectories.length === 0) {
    return
  }

  temporaryDirectories.map((file) => {
    fs.rm(file, { force: true, recursive: true }, (error) => {
      if (error) {
        // eslint-disable-next-line no-console
        console.error(error)
      }
    })
  })
})
